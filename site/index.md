@def title = "Welcome"
@def tags = ["syntax", "code"]

# Welcome to the World of Multivariable Calculus! 

 
<!-- \tableofcontents  you can use \toc as well -->

**_MATH 2450: Most important math class you will ever take!_**

![Alt Text](/assets/toronto.gif)

---
## Things to be known:

Exam 1: Sep. 17 [Exam info lives here!](https://tinyurl.com/2b9at83f)


---

[Anonymous Lecture Feedback](https://forms.gle/yPHz4HKHtoSXaczY7)

---

* Head over to the [WebWork](https://webwork.math.ttu.edu/webwork2/f21bathukorm2450sH01/)
* Head over to the [Blackboard](https://ttu.blackboard.com/)

---



Skeleton notes [lives here!](https://tinyurl.com/2fftr5b8)

Course syllabus [lives here!](https://tinyurl.com/rhuv9vr4)


* **Lecture:** MWF: 12:00- 1:15, MATH 011
    - Live class session via ZOOM: [Click here!!!](https://zoom.us/j/8065430626)  
* **Instructor:** Bhagya Athukorallage, PhD
* **Office Hours:**  MWF: 11:00am - 11:50am, Th: 1:00pm - 1:50pm, & F: 9:00-9:50am or by appointments
    - [Click here to enter my ZOOM office](https://zoom.us/j/8065430626)
* **Office:** MATH 253
* **E-mail:** bhagya.athukorala@ttu.edu







