@def title = "WebWork"

# Homework

![Alt Text](/assets/HW.jpg)


## WebWork assignments

* [HW 4](https://webwork.math.ttu.edu/webwork2/f21bathukorm2450sH01/) is posted. Due on Friday, Sep. 17th.
* [HW 3](https://webwork.math.ttu.edu/webwork2/f21bathukorm2450sH01/) is posted. Due on Monday, Sep. 13th. 
* [HW 2](https://webwork.math.ttu.edu/webwork2/f21bathukorm2450sH01/) is posted. Due on Friday, Sep. 3rd. 
* [HW 1](https://webwork.math.ttu.edu/webwork2/f21bathukorm2450sH01/) is posted. Due on Tuesday, Aug. 31st. 




## Useful WeBWork Info

* Head over to the [WebWork](https://webwork.math.ttu.edu/webwork2/spr21bathukorm2450s012/) 
* [Click here](https://drive.google.com/file/d/1dbAEspqW6VyGxjR9nSszjoPdFLXmZdbq/view?usp=sharing) to see a tutorial on how to use WeBWork
* [Click here](https://drive.google.com/file/d/1bzmKCZdGB2-xVwkSrNj16bxw4qKGWTin/view?usp=sharing) to read the tutorial on entering answers in WeBWork





