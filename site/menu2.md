@def title = "Class Notes"

# Class Notes

![Alt Text](/assets/notes3.jpg)


## Week 14

 - [Chapter 13.7](https://tinyurl.com/52657ee3)

## Week 13

 - [Chapter 13.6](https://tinyurl.com/yckzchvm). 

 - [Chapter 13.5](https://drive.google.com/open?id=1qY_Pu9S9ZHTaLyFR-1nKxPUWTqxpi0QA). 



## Week 12

* Friday 
    - [Exam 3]()

* Wednesday 
    - [Chapter 13.4](https://tinyurl.com/yx5nywhj)

* Monday 
    - [Chapter 13.3 (part 2)](https://tinyurl.com/7fp3fv3s)


## Week 11

* Friday 
    - [Chapter 13.3 (part 1)](https://tinyurl.com/7fp3fv3s)
    - [Chapter 13.2 (part 2)](https://tinyurl.com/4uutctvf)

* Wednesday 
    - [Chapter 13.2 (part 1)](https://tinyurl.com/4uutctvf)

* Monday 
    - [Chapter 13.1 ](https://tinyurl.com/n2k3haca)


## Week 10

* Friday 
    - [Chapter 12.8](https://tinyurl.com/3m37f4b7)

* Wednesday 
    - [Chapter 12.7 (part 2)](https://tinyurl.com/3x8tusbn)

* Monday 
    - [Chapter 12.7 (part 1)](https://tinyurl.com/kfcyzvy8)


## Week 9

* Friday 
    - [Chapter 12.5 (part 2)](https://tinyurl.com/kwywwbam)

* Wednesday 
    - [Chapter 12.5 (part 1)](https://tinyurl.com/kwywwbam)

* Monday 
    - [Chapter 12.4](https://tinyurl.com/k5dexdfn)

## Week 8

* Friday 
    - [Chapter 12.3](https://tinyurl.com/ypxdspc8)

* Wednesday 
    - [Chapter 12.2](https://tinyurl.com/ynyasc5d)

* Monday 
    - [Chapter 12.1](https://tinyurl.com/hj636unn)



## Week 7

* Friday 
    - [Chapter 11.8](https://tinyurl.com/3zkj77y6)

* Wednesday 
    - [Chapter 11.7](https://tinyurl.com/ky9b2s7r)
    - [Chapter 11.6 (part2)](https://tinyurl.com/8nz25tjc)

* Monday 
    - [Chapter 11.6 (part1)](https://tinyurl.com/8nz25tjc)
    - [Chapter 11.4](https://tinyurl.com/9utu9stn)



## Week 6


* Monday, Sep. 20th
    - [Chapter 11.6 (part1)](https://tinyurl.com/8nz25tjc)
    - [Chapter 11.4](https://tinyurl.com/9utu9stn)


## Week 5

* Friday, Sep. 24th
    - [Chapter 11.5](https://tinyurl.com/5ft93jd6)


* Wednesday, Sep. 22nd
    - [Chapter 11.3](https://tinyurl.com/4bb438e6)


* Monday, Sep. 20th
    - [Chapter 11.2 (part 2)](https://tinyurl.com/zepkswka)


## Week 4

* Friday, Sep. 17th
    - [Chapter 11.2 (part 1)](https://tinyurl.com/zepkswka)

* Wednesday, Sep. 15th
    - [Chapter 11.1](https://tinyurl.com/2armrt6n)

* Exam 1 review lives [here!](https://youtu.be/RLEYRZrxXkQ)

* Monday, Sep. 13th
    - [Chapter 9.7](https://tinyurl.com/pt4m39wc)
    - [Chapter 10.4 (part 2)](https://tinyurl.com/3xujk77d)


## Week 3 

* Friday, Sep. 10th
    - [Chapter 10.4 (part1)](https://tinyurl.com/3xujk77d)
    - [Chapter 10.2 (part2)](https://tinyurl.com/2armrt6n)

* Wednesday, Sep. 8th
    - [Chapter 10.2 (part 1)](https://tinyurl.com/2armrt6n)



## Week 2 

* Friday, Sep. 3rd
    - [Chapter 10.1](https://tinyurl.com/46tnwat4)

* Wednesday, Sep. 1st
    - [Chapter 9.6 (part 2)](https://tinyurl.com/sv2rch24)

* Monday, Aug. 30th
    - [Chapter 9.6 (part 1)](https://tinyurl.com/sv2rch24)
    - [Chapter 9.5 (part 2)](https://tinyurl.com/yx2rxu8x)



## Week 1 

* Friday, Aug. 27th
    - [Chapter 9.5 (part 1)](https://tinyurl.com/yx2rxu8x)
    
* Wednesday, Aug. 25th
    - [Review: 9.1-9.4](https://tinyurl.com/axzbstte)

* Monday, Aug. 23rd
    - [Welcome slides!](https://tinyurl.com/kjfxv3az)



