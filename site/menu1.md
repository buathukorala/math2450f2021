@def title = "Announcements"

# Announcements

![Alt Text](/assets/news.jpg)


## Week 4 

* HW 5 is posted in WeBWork: Due on Friday, Sep. 17th.

## Week 3 

* HW 4 is posted in WeBWork: Due on Friday, Sep. 17th.


## Week 2 

* HW 3 is posted in WeBWork: Due on Monday, Sep. 13th.

* HW 2 is posted in WeBWork: Due on Tuesday, Sep. 3rd. 

## Week 1 

* Install [Proctorio Chrome Extension](https://getproctorio.com/) 
     @@colbox-blue
     [Proctorio Quick Start Guide](https://drive.google.com/file/d/1L3AUF83Pq3Wz2aVIw6mVoTYKpWG1uEc7/view?usp=sharing)
     @@

* HW 1 is posted in WeBWork: Due on Tuesday, Aug. 31st. 




